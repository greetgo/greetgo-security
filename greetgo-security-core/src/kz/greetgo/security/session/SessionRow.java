package kz.greetgo.security.session;

import java.util.Date;

public class SessionRow implements SessionParams {
  public final String token;
  public final Object sessionData;
  public final Date   insertedAt;
  public final Date   lastTouchedAt;
  public final String samenessId;

  public SessionRow(String token, Object sessionData, Date insertedAt, Date lastTouchedAt, String samenessId) {
    this.token         = token;
    this.sessionData   = sessionData;
    this.insertedAt    = insertedAt;
    this.lastTouchedAt = lastTouchedAt;
    this.samenessId    = samenessId;
  }

  @Override
  public Date insertedAt() {
    return insertedAt;
  }

  @Override
  public Date lastTouchedAt() {
    return lastTouchedAt;
  }
}
